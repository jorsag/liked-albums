import { combineReducers } from 'redux';
import api from '../api';

/**
 * ACTION TYPES
 */
const ARTIST_LOOKUP_FINISHED = 'ARTIST_LOOKUP_FINISHED';
const ALBUMS_BY_ARTIST_FETCH_FINISHED = 'ALBUMS_BY_ARTIST_FETCH_FINISHED';
const LIKED_ALBUMS_FETCH_FINISHED = 'LIKED_ALBUMS_FETCH_FINISHED';
const TOGGLE_LIKE_ALBUM = 'TOGGLE_LIKE_ALBUM';

/**
 * ACTIONS
 */
const handleError = entity => err => alert(`ERROR fetching ${entity}: ${JSON.stringify(err)}`);
const extractAlbum = jsonAlbum => ({
  id: jsonAlbum.collectionId,
  name: jsonAlbum.collectionName,
  artistId: jsonAlbum.artistId,
  artistName: jsonAlbum.artistName,
  collectionPrice: jsonAlbum.collectionPrice,
  contentAdvisoryRating: jsonAlbum.contentAdvisoryRating,
  trackCount: jsonAlbum.trackCount,
  copyright: jsonAlbum.copyright,
  country: jsonAlbum.country,
  currency: jsonAlbum.currency,
  releaseDate: jsonAlbum.releaseDate,
  primaryGenreName: jsonAlbum.primaryGenreName,
});

export const searchArtists = searchTerm => dispatch =>
  api
    .searchArtists(searchTerm)
    .then(json => {
      const extractArtist = jsonArtist => ({
        id: jsonArtist.artistId,
        name: jsonArtist.artistName,
      });
      const displayedArtists = json.data.results.map(extractArtist);
      dispatch({ type: ARTIST_LOOKUP_FINISHED, displayedArtists });
    })
    .catch(handleError('artists'));

export const navigateToAlbums = (navigation, artistId) => (dispatch, getState) => {
  const artistName = selectArtistName(getState(), artistId);

  const dispatchAndNavigate = json => {
    const albumsOnly = collection =>
      collection.wrapperType === 'collection' && collection.collectionType === 'Album';
    let albumCache = json.data.results.filter(albumsOnly).map(extractAlbum);
    dispatch({ type: ALBUMS_BY_ARTIST_FETCH_FINISHED, artistId, albumCache });

    albumCache = selectAlbumCache(getState())[artistId];
    navigation.navigate('Albums', { artistName, albumCache });
  };

  if (isArtistCached(getState(), artistId)) {
    let albumCache = selectAlbumCache(getState())[artistId];
    navigation.navigate('Albums', { artistName, albumCache });
  } else {
    api
      .getAlbumsByArtist(artistId)
      .then(dispatchAndNavigate)
      .catch(handleError('albums by artist'));
  }
};

export const fetchLikedAlbums = () => (dispatch, getState) =>
  api
    .getLikedAlbums(selectLikedAlbums(getState()))
    .then(json => {
      const likedAlbums = json.data.results.map(extractAlbum);
      dispatch({ type: LIKED_ALBUMS_FETCH_FINISHED, likedAlbums });
    })
    .catch(handleError('liked albums'));

export const toggleLikeAlbum = albumId => ({ type: TOGGLE_LIKE_ALBUM, albumId });

/**
 * REDUCERS
 */
const initialState = {
  displayedArtists: [],
  albumCache: {},
  artistsWithAllAlbumsCached: [],
  likedAlbums: [],
};

const displayedArtists = (displayedArtists = initialState.displayedArtists, action) => {
  switch (action.type) {
    case ARTIST_LOOKUP_FINISHED:
      return action.displayedArtists;
    default:
      return displayedArtists;
  }
};

const cacheAlbum = (cache, album) => {
  const isAlbumCached =
    cache[album.artistId] &&
    cache[album.artistId].filter(cachedAlbum => cachedAlbum.id === album.id).length > 0;
  if (!isAlbumCached) {
    return {
      ...cache,
      [album.artistId]: [...(cache[album.artistId] || []), album],
    };
  }
  return cache;
};

const albumCache = (albumCache = initialState.albumCache, action) => {
  switch (action.type) {
    case ALBUMS_BY_ARTIST_FETCH_FINISHED:
      return { ...albumCache, [action.artistId]: action.albumCache };
    case LIKED_ALBUMS_FETCH_FINISHED:
      return action.likedAlbums.reduce(cacheAlbum, albumCache);
    default:
      return albumCache;
  }
};

const artistsWithAllAlbumsCached = (
  artistsWithAllAlbumsCached = initialState.artistsWithAllAlbumsCached,
  action
) => {
  switch (action.type) {
    case ALBUMS_BY_ARTIST_FETCH_FINISHED:
      return artistsWithAllAlbumsCached.includes(action.artistId)
        ? artistsWithAllAlbumsCached
        : [...artistsWithAllAlbumsCached, action.artistId];
    default:
      return artistsWithAllAlbumsCached;
  }
};

export const likedAlbums = (likedAlbums = initialState.likedAlbums, action) => {
  switch (action.type) {
    case TOGGLE_LIKE_ALBUM:
      return likedAlbums.includes(action.albumId)
        ? likedAlbums.filter(id => id !== action.albumId)
        : [...likedAlbums, action.albumId];
    default:
      return likedAlbums;
  }
};

export default combineReducers({
  displayedArtists,
  albumCache,
  artistsWithAllAlbumsCached,
});

/**
 * SELECTORS
 */
export const selectDisplayedArtists = state => state.music.displayedArtists;
export const selectArtistName = (state, artistId) => {
  const displayedArtists = selectDisplayedArtists(state).filter(artist => artist.id === artistId);
  return displayedArtists.length === 1 ? displayedArtists[0].name : null;
};
export const selectAlbumCache = state => state.music.albumCache;
export const selectLikedAlbums = state => state.likedAlbums;
export const isAlbumLiked = (state, albumId) => selectLikedAlbums(state).includes(albumId);
export const isArtistCached = (state, artistId) =>
  state.music.artistsWithAllAlbumsCached.includes(artistId);
