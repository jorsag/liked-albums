import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, ScrollView } from 'react-native';
import { ListItem, List } from 'react-native-elements';
import { toggleLikeAlbum, isAlbumLiked } from '../../../features/music/ducks';

const mapStateToProps = state => ({ state });
const mapDispatchToProps = dispatch => ({
  toggleLikeAlbum: bindActionCreators(toggleLikeAlbum, dispatch),
});

@connect(mapStateToProps, mapDispatchToProps)
export default class AlbumsContainer extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: `${navigation.state.params.artistName} Albums`,
  });

  getIconNameByLike = albumId =>
    isAlbumLiked(this.props.state, albumId) ? 'favorite' : 'favorite-border';

  render() {
    const albumCache = this.props.navigation.state.params.albumCache;
    const album2Component = album => (
      <ListItem
        key={album.id}
        title={album.name}
        rightIcon={{ name: this.getIconNameByLike(album.id) }}
        onPressRightIcon={() => this.props.toggleLikeAlbum(album.id)}
      />
    );
    const albumComponents = albumCache.map(album2Component);

    return (
      <View>
        <ScrollView>
          <List>{albumComponents}</List>
        </ScrollView>
      </View>
    );
  }
}
