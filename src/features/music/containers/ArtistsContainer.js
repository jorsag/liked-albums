import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  selectDisplayedArtists,
  searchArtists,
  navigateToAlbums,
} from '../../../features/music/ducks';
import { View, ScrollView } from 'react-native';
import { SearchBar, ListItem, List } from 'react-native-elements';

const mapStateToProps = state => ({ displayedArtists: selectDisplayedArtists(state) });
const mapDispatchToProps = dispatch => ({
  searchArtists: bindActionCreators(searchArtists, dispatch),
  navigateToAlbums: bindActionCreators(navigateToAlbums, dispatch),
});

@connect(mapStateToProps, mapDispatchToProps)
export default class ArtistsContainer extends Component {
  static navigationOptions = { title: 'Artists' };

  render() {
    const { navigation, searchArtists, displayedArtists, navigateToAlbums } = this.props;

    const artistNameComparator = (artist1, artist2) => artist1.name.localeCompare(artist2.name);
    const artist2Component = artist => (
      <ListItem
        onPress={() => navigateToAlbums(navigation, artist.id)}
        key={artist.id}
        title={artist.name}
      />
    );
    const artistsComponents = displayedArtists.sort(artistNameComparator).map(artist2Component);

    return (
      <View>
        <SearchBar lightTheme onChangeText={searchArtists} placeholder="artist name here" />
        <ScrollView>
          <List>{artistsComponents}</List>
        </ScrollView>
      </View>
    );
  }
}
