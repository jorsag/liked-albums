import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, ScrollView, Text, Button } from 'react-native';
import { ListItem, List } from 'react-native-elements';
import {
  fetchLikedAlbums,
  selectLikedAlbums,
  selectAlbumCache,
} from '../../../features/music/ducks';

const mapStateToProps = state => ({
  likedAlbums: selectLikedAlbums(state),
  albumCache: selectAlbumCache(state),
});
const mapDispatchToProps = dispatch => ({
  fetchLikedAlbums: bindActionCreators(fetchLikedAlbums, dispatch),
});

@connect(mapStateToProps, mapDispatchToProps)
export default class LikedAlbumsContainer extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Liked Albums',
    headerRight: <Button title={'Select'} onPress={() => navigation.navigate('Artists')} />,
  });

  componentDidMount = () => this.props.fetchLikedAlbums();

  render() {
    const { albumCache, likedAlbums, navigation } = this.props;
    const likedOnly = album => likedAlbums.includes(album.id);
    const flattenCachedAlbums = () => {
      const artistIds = Object.keys(albumCache) || [];
      return artistIds.reduce((albums, artistId) => [...albums, ...albumCache[artistId]], []);
    };
    const album2Component = album => (
      <ListItem
        key={album.id}
        title={album.name}
        subtitle={album.artistName}
        onPress={() => navigation.navigate('AlbumDetails', { album })}
      />
    );

    const albumComponents = flattenCachedAlbums()
      .filter(likedOnly)
      .map(album2Component);

    return (
      <View>
        <ScrollView>
          <List>{albumComponents}</List>
        </ScrollView>
      </View>
    );
  }
}
