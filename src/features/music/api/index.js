import apiClient from '../../../common/services/apiClient';

export default {
  searchArtists: searchTerm => apiClient.get(`/search?term=${searchTerm}&entity=musicArtist`),
  getAlbumsByArtist: artistId =>
    apiClient.get(`https://itunes.apple.com/lookup?id=${artistId}&entity=album`),
  getLikedAlbums: likedAlbums =>
    apiClient.get(`https://itunes.apple.com/lookup?id=${likedAlbums.join(',')}`),
};
