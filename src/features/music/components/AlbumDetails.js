import React, { Component } from 'react';
import { View, ScrollView, Text } from 'react-native';
import { ListItem, List } from 'react-native-elements';

export default class AlbumDetails extends Component {
  static navigationOptions = ({ navigation }) => {
    const { artistName, name } = navigation.state.params.album;
    return { title: `${artistName} - ${name}` };
  };

  render() {
    const {
      collectionPrice,
      currency,
      contentAdvisoryRating,
      trackCount,
      copyright,
      country,
      releaseDate,
      primaryGenreName,
    } = this.props.navigation.state.params.album;

    return (
      <View>
        <Text>
          collectionPrice: {collectionPrice} {currency}
        </Text>
        <Text>contentAdvisoryRating: {contentAdvisoryRating}</Text>
        <Text>trackCount: {trackCount}</Text>
        <Text>copyright: {copyright}</Text>
        <Text>country: {country}</Text>
        <Text>releaseDate: {releaseDate}</Text>
        <Text>primaryGenreName: {primaryGenreName}</Text>
      </View>
    );
  }
}
