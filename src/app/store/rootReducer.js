import { combineReducers } from 'redux';
import auth, { LOGOUT } from '../../features/auth/ducks';
import spinner from '../../features/spinner/ducks';
import music, { likedAlbums } from '../../features/music/ducks';

const rootReducer = combineReducers({
  auth,
  spinner,
  music,
  // HACK: likedAlbums is not in music, because whitelisting in redux-persists works with top level state components
  // only
  likedAlbums,
});

export default (state, action) => {
  if (action.type === LOGOUT) {
    return rootReducer(undefined, action);
  }

  return rootReducer(state, action);
};
