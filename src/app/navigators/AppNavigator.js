import React from 'react';
import { StackNavigator } from 'react-navigation';
import {
  ArtistsContainer,
  AlbumsContainer,
  LikedAlbumsContainer,
} from '../../features/music/containers';
import { AlbumDetails } from '../../features/music/components';

export default StackNavigator(
  {
    Artists: { screen: ArtistsContainer },
    Albums: { screen: AlbumsContainer },
    LikedAlbums: { screen: LikedAlbumsContainer },
    AlbumDetails: { screen: AlbumDetails },
  },
  {
    initialRouteName: 'LikedAlbums',
  }
);
